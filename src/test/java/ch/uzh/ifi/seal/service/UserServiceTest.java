package ch.uzh.ifi.seal.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import ch.uzh.ifi.seal.Application;
import ch.uzh.ifi.seal.constant.UserStatus;
import ch.uzh.ifi.seal.entity.User;
import ch.uzh.ifi.seal.repository.UserRepository;

/**
 * Test class for the UserResource REST resource.
 *
 * @see UserService
 */
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class)
public class UserServiceTest {

    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Test
    public void createUser() {
        Assertions.assertNull(userRepository.findByUsername("testUsername"));

        User testUser = new User();
        testUser.setName("testName");
        testUser.setUsername("testUsername");

        User createdUser = userService.createUser(testUser);

        Assertions.assertNotNull(createdUser.getToken());
        Assertions.assertEquals(UserStatus.ONLINE, createdUser.getStatus());
        Assertions.assertEquals(createdUser, userRepository.findByToken(createdUser.getToken()));
    }
}
