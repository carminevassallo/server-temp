# Server-Template
Build a DevOps pipeline with GitLab

## Project Badges

What's the state of the pipeline?
[![pipeline status](https://gitlab.com/carminevassallo/server-temp/badges/master/pipeline.svg)](https://gitlab.com/carminevassallo/server-temp/-/commits/master)

How many bugs in my code?
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=carminevassallo_server-temp&metric=bugs)](https://sonarcloud.io/dashboard?id=carminevassallo_server-temp)

How many code smells did I introduce?
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=carminevassallo_server-temp&metric=code_smells)](https://sonarcloud.io/dashboard?id=carminevassallo_server-temp)

Is quality of my code enough to release my application?
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=carminevassallo_server-temp&metric=alert_status)](https://sonarcloud.io/dashboard?id=carminevassallo_server-temp)